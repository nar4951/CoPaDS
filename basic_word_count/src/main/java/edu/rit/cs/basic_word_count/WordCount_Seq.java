package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WordCount_Seq {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";


    /*
        Line reader
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();                                 //Gets line to read (first line)

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {              //While the lines still exist get next one
                allReviews.add(new AmazonFineFoodReview(reviewLine));   //Adds line to list called allReviews
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;                                              //Return list of all lines
    }


    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String[] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file); //Get all lines

        /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }

        MyTimer myTimer = new MyTimer("wordCount");                                 //Setup timer
        myTimer.start_timer();                                                               //StartTimer
        /* Tokenize words */
        List<String> words = new ArrayList<String>();                                           //List of words
        for(AmazonFineFoodReview review : allReviews) {                                    //For each line
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");                              //? Remove all but a-zA-Z
            Matcher matcher = pattern.matcher(review.get_Summary());                       //^^^

            while(matcher.find())                                                          //While words still exist
                words.add(matcher.group().toLowerCase());                                  //Add word to final list
        }

//        /* Count words */
        Map<String, Integer> wordcount = new HashMap<>(); ///////////////////////////big sort
        for(String word : words) {                                                         //For every word
            if(!wordcount.containsKey(word)) {                                             //If not in hashmap
                wordcount.put(word, 1);                                                    //Add to hashmap
            } else{
                int init_value = wordcount.get(word);                                      //Get word
                wordcount.replace(word, init_value, init_value+1);                 //Increase count
            }
        }
        myTimer.stop_timer();                                                               //End timer

        print_word_count(wordcount);                                                        //Print words

        myTimer.print_elapsed_time();                                                       //Print timer
    }

}
