package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCount_Threads {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";


    /*
        Line reader
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();                                 //Gets line to read (first line)

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {              //While the lines still exist get next one
                allReviews.add(new AmazonFineFoodReview(reviewLine));   //Adds line to list called allReviews
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;                                              //Return list of all lines
    }


    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String[] args) throws Exception {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file); //Get all lines
        MyTimer myTimer = new MyTimer("wordCount");                                 //Setup timer
        myTimer.start_timer();                                                               //StartTimer

        FutureTask[] reviewRead = new FutureTask[8];

        //System.out.println("ReviewSize: " + allReviews.size());
        int reviewSize = (allReviews.size()/8);
        //System.out.println("ReviewSize/8: " + reviewSize);

        //Gather threads

        for(int i = 0; i < 8; i++) {
            List<AmazonFineFoodReview> partition = new ArrayList();
            if(i == 7) {
                int extra = (reviewSize%4);
                partition.addAll((allReviews.subList(i * reviewSize, extra + ((i + 1) * reviewSize) - 1)));
                //System.out.println("Start: " + (i*reviewSize) + " Finish: " + (((i+1)*reviewSize)-1+extra));
            }
            else{
                partition.addAll((allReviews.subList(i * reviewSize, ((i + 1) * reviewSize) - 1)));
                //System.out.println("Start: " + (i*reviewSize) + " Finish: " + (((i+1)*reviewSize)-1));
            }

            //System.out.println(partition);
            MyThread thread = new MyThread(partition);
            reviewRead[i] = new FutureTask(thread);
            Thread t = new Thread(reviewRead[i]);
            t.start();
        }

        Map<String,Integer> words = new TreeMap<>();
        for(int i = 0; i < 8; i++){
            TreeMap<String,Integer> mergeMap = (TreeMap<String, Integer>)(reviewRead[i].get());
            mergeMap.forEach((k,v) -> words.merge(k,v, (a,b) -> a+b));
        }

        myTimer.stop_timer();                                                               //End timer
        //print_word_count(words);                                                        //Print words
        myTimer.print_elapsed_time();                                                       //Print timer
    }

}

class MyThread implements Callable {
    private List<AmazonFineFoodReview> reviews;

    public MyThread(List<AmazonFineFoodReview> reviews){
        this.reviews = reviews;
    }

    public Map call(){
        // Tokenize words
        Map<String, Integer> wordcount = new TreeMap<>();
        List<String> words1 = new ArrayList<String>();
        Pattern pattern = Pattern.compile("([a-zA-Z]+)"); //? Remove all but a-zA-Z
        for(AmazonFineFoodReview review : reviews) {                                    //For each line
            Matcher matcher = pattern.matcher(review.get_Summary());                       //^^^
            while(matcher.find())                                                          //While words still exist
                words1.add(matcher.group().toLowerCase());                            //Add word to final list
                //System.out.println(words1);
        }
        //Count words
        //Tree for sort instead of hash
        for(String word : words1) {                                                         //For every word
            if(!wordcount.containsKey(word)) {                                             //If not in hashmap
                wordcount.put(word, 1);                                                    //Add to hashmap
            } else{
                int init_value = wordcount.get(word);                                      //Get word
                wordcount.replace(word, init_value, init_value+1);                 //Increase count
            }
        }
        return wordcount;
    }

}

