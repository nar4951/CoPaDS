package edu.rit.cs.basic_word_count;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCount_Cluster_worker {
    public static void main (String args[]) {
        /*
            Arguments supply message and hostname of destination
        */
        String server_address = args[0];
        Socket s = null;
        try{
            /*
                Open up socket
            */
            int serverPort = 7896;
            s = new Socket(server_address, serverPort);
            System.out.println("Socket created");

            /*
                Set up streams
            */
            DataInputStream in     =    new DataInputStream( s.getInputStream());
            DataOutputStream out   =    new DataOutputStream(s.getOutputStream());
            ObjectInputStream ois  =    new ObjectInputStream(in);
            ObjectOutputStream oos =    new ObjectOutputStream(out);
            System.out.println("Oio created");

            /*
                Setup reviews, wordcounter and word list
            */
            Map<String, Integer> wordcount = new TreeMap<>();
            ArrayList<String> reviews = (ArrayList<String>)ois.readObject();
            List<String> words = new ArrayList<String>();

            /*
                Read the strings
            */
            System.out.println("Sorting words");
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            for(String review : reviews) {
                Matcher matcher = pattern.matcher(review);
                while(matcher.find())
                    words.add(matcher.group().toLowerCase());
            }
            for(String word : words) {
                if(!wordcount.containsKey(word)) {
                    wordcount.put(word, 1);
                } else{
                    int init_value = wordcount.get(word);
                    wordcount.replace(word, init_value, init_value+1);
                }
            }
            System.out.println("Wordcount done!");

            /*
                Send tree back and close connection
            */
            oos.writeObject(wordcount);

        }catch (UnknownHostException e){
            System.out.println("Wrk+Sock:"+e.getMessage());
        }catch (EOFException e){
            System.out.println("Wrk+EOF:"+e.getMessage());
        }catch (IOException e){
            System.out.println("Wrk+IO:"+e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Wrk+CNF:"+e.getMessage());
        } finally {
            if(s!=null)
                try {
                s.close();
                System.out.println("Socket closed, work done!");
            }catch (IOException e){
                System.out.println("close:"+e.getMessage());
            }
        }
    }

}
