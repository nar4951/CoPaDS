package edu.rit.cs.basic_word_count;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class WordCount_Cluster_master {

    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";

    //Line Reader
    public static List<String> read_reviews(String dataset_file) {
        List<String> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();                                 //Gets line to read (first line)

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {              //While the lines still exist get next one
                allReviews.add(new AmazonFineFoodReview(reviewLine).get_Summary());   //Adds line to list called allReviews
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;                                              //Return list of all lines
    }
    //Print words
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static ArrayList<String> parition_creation(int nodeCreate, int reviewSize, List<String> allReviews){
        ArrayList<String> partition = new ArrayList<String>();
        System.out.println("Node"+nodeCreate+" created!");
        if(nodeCreate == 4){
            int extra = (reviewSize%5);
            partition.addAll((allReviews.subList(nodeCreate * reviewSize, extra + ((nodeCreate + 1) * reviewSize) - 1)));
        }
        else{
            partition.addAll((allReviews.subList(nodeCreate * reviewSize, ((nodeCreate + 1) * reviewSize) - 1)));
        }
        return partition;
    }

    public static void main(String args[]) {

        List<String> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file); //Get all lines
        MyTimer myTimer = new MyTimer("wordCount");                                 //Setup timer
        FutureTask[] nodeThreads = new FutureTask[5];
        int reviewSize = (allReviews.size()/5);

        try {
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            /*
                Loop creating nodes and distributing them as nodes connect
             */
            int nodeCreate = 0;
            while (nodeCreate < 5) {
                System.out.println("Waiting for worker connection");
                Socket clientSocket = listenSocket.accept();
                System.out.println("Got one!");
                if(nodeCreate == 0){
                    myTimer.start_timer();
                }
                /*
                    Create a connection as nodes connect and send them partitions
                */
                Connection c = new Connection(clientSocket, parition_creation(nodeCreate, reviewSize, allReviews));
                nodeThreads[nodeCreate] = new FutureTask(c);
                Thread t = new Thread(nodeThreads[nodeCreate]);
                t.start();
                System.out.println("Node"+nodeCreate+ "started");
                nodeCreate++;
            }
        } catch (IOException e) {
            System.out.println("Listen:" + e.getMessage());
        }

        /*
            Retrieve the results of the nodes and combine them
        */
        Map<String,Integer> words = new TreeMap<>();
        for(int i = 0; i < 5; i++){
            TreeMap<String,Integer> mergeMap = null;
            try {
                mergeMap = (TreeMap<String, Integer>)(nodeThreads[i].get());
            } catch (InterruptedException e) {
                System.out.println("Merge+IE:" + e.getMessage());
            } catch (ExecutionException e) {
                System.out.println("Merge+EE:" + e.getMessage());
            }
            mergeMap.forEach((k,v) -> words.merge(k,v, (a,b) -> a+b));
        }

        /*
            Stop timer and print word count
        */
        myTimer.stop_timer();
        print_word_count(words);
        myTimer.print_elapsed_time();

    }
}
class Connection implements Callable {
    DataInputStream in;
    DataOutputStream out;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    Socket clientSocket;
    private List<String> reviews;

    public Connection(Socket aClientSocket, ArrayList<String> partition) {
        this.reviews = partition;
        try {
            System.out.println("Creating streams");
            clientSocket = aClientSocket;
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
            oos = new ObjectOutputStream(out);
            ois = new ObjectInputStream(in);
            System.out.println("Streams created");
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public Map call() {
        System.out.println("Connection established!");
        Map<String, Integer> wordcount = null;
        try {                 // an echo server
            oos.writeObject(reviews);
            System.out.println("Partition sent.");
            wordcount = (TreeMap<String, Integer>)ois.readObject();
            System.out.println("Tree received.");
        } catch (EOFException e) {
            System.out.println("Call+EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("Call+IO:" + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Call+CNF:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
        return wordcount;
    }
}

